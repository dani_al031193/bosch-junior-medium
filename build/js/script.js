// search
function searchOpen () {
    document.querySelector('.search--open').classList.toggle('show');
    search.classList.toggle('hide');
}
var search = document.querySelector('.search');
search.addEventListener('click', searchOpen);
document.querySelector('.search--close').addEventListener('click', searchOpen)

// options
$('.options__item').click(function() {
    $('.options__item').removeClass('active');
    $(this).addClass('active')
});

// change price
function changeText(text) {
    document.querySelector('#purchase__price').innerHTML = text;
}

// counter
function counterUp() {
    var up = parseInt(document.querySelector('.offers__control').value);
    document.querySelector('.offers__control').value = up + 1;
}

function counterDown() {
    var down = parseInt(document.querySelector('.offers__control').value);
    document.querySelector('.offers__control').value = down - 1;
}

var buttonUp = document.querySelector('.offers__buttons--top');
buttonUp.addEventListener('click', counterUp);

var buttonDown = document.querySelector('.offers__buttons--bottom');
buttonDown.addEventListener('click', counterDown);

// burger
function burgerOpenOverlay () {
    document.querySelector('.navigation').classList.toggle('openIMP');
    document.querySelector('#overlay').classList.toggle('overlay');
    document.querySelector('.overlay').classList.toggle('openIMP'); //
    document.querySelector('.overlay__icon').classList.toggle('openIMP');
    servicesOpen ();    
}
var burger = document.querySelector('.menu__burger');
burger.addEventListener('click', burgerOpenOverlay);
function burgerHideOverlay () {
    document.querySelector('.navigation').classList.remove('openIMP');
    document.querySelector('.overlay').classList.remove('openIMP');
    document.querySelector('#overlay').classList.remove('overlay');
    document.querySelector('.overlay__icon').classList.remove('openIMP');
}
document.querySelector('.overlay__icon').addEventListener('click', burgerHideOverlay);

// navigation
var el = document.getElementsByClassName('trigger');
// Дефолтная загрузка
if (window.innerWidth > 768) {
  setEvent(el);
} else {
  remEvent(el);
  setEvent(el, "click", "dblclick");
}
// Проверка если окно поменяло размер
window.addEventListener('resize', function(e) {

  console.log('Resized', this.innerWidth, this.innerHeight);
  if (this.innerWidth <= 768) { // Your size
    // удаляет события
    remEvent(el);
    //------------------------------------------------------------
    // меняет события
    setEvent(el, "click", "dblclick");
  } else {
    setEvent(el);
  }
})
//----------------------------------------------------------------
function setEvent(elem, e1 = "mouseenter", e2 = "mouseleave", flg = false) {
    for (var i = 0; i < elem.length; i++) {
      elem[i].addEventListener(e1, showSub, flg);
      elem[i].addEventListener(e2, hideSub, flg);
    }
}
  
function remEvent(elem, e1 = "mouseenter", e2 = "mouseleave", ) {
    for (var i = 0; i < elem.length; i++) {
      // Remove 
      elem[i].removeEventListener(e1, showSub);
      elem[i].removeEventListener(e2, hideSub);
  
    }
}
//----------------------------------------------------------------
function showSub(e) {
    if(this.children.length>1) {
       this.children[1].style.height = "auto";
       this.children[1].style.overflow = "visible";
       this.children[1].style.opacity = "1";
    } else {
       return false;
    }
};

function hideSub(e) {
    if(this.children.length>1) {
        this.children[1].style.height = "0px";
        this.children[1].style.overflow = "hidden";
        this.children[1].style.opacity = "0";
    } else {
       return false;
    }
};